#!/usr/bin/python3

import click
import sys
import fluentpy as _
from pathlib import Path
import math
import subprocess
import requests
from ChamanApi import ChamanApi
import websocket
import json
import os
import time

from PrinterConfig import PrinterConfig


@click.command(context_settings={"ignore_unknown_options": True})
@click.option('--printer-path', type=click.Path())
@click.option('--chaman-base-url', type=str)
@click.option('--chaman-token', type=str)
@click.option('--chaman-printer-id', type=str)
@click.option('--cups-printer', type=str)
@click.option('--sticker-width', type=str)
@click.option('--sticker-height', type=str)
@click.option('--cups-option', type=(str,str), multiple=True)
def launch(printer_path, chaman_base_url, chaman_token, chaman_printer_id, cups_printer,  sticker_width, sticker_height, cups_option):
    chamanApi = ChamanApi(chaman_base_url, chaman_token, chaman_printer_id, sticker_width, sticker_height)
    cupsOptions = {}
    for i in cups_option:
        cupsOptions[i[0]] = i[1]

    printerConfig = PrinterConfig(cups_printer, cupsOptions)

    def on_message(ws, message):
        if(json.loads(message)['eventType'] == 'Print'):
            retrieveAndPrint(chamanApi, printerConfig)

    def reconnect(ws, code, reason):
        print("reconnect in 1 seconds")
        time.sleep(1)
        retrieveAndPrint(chamanApi, printerConfig)
        launchws()


    def launchws():
        ws = chamanApi.webSocket(on_message=on_message, on_close=reconnect)
        ws.run_forever()

    retrieveAndPrint(chamanApi, printerConfig)
    launchws()


def retrieveAndPrint(chamanApi, printerConfig):
    printQueue = chamanApi.getPrintQueue()

    print("Print queue retrieved, " + str(len(printQueue)) + " print jobs")
    _(printQueue).map(lambda v: printJob(chamanApi,printerConfig, v))


def printJob(chamanApi,printerConfig ,job):
    print("retrieve sticker for " + str(job))
    chamanApi.getSticker(job)
    printerConfig.print()
    chamanApi.markPrinted(job)


if __name__ == '__main__':
    launch()
