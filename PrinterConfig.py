#!/usr/bin/python3

import click
import sys
import fluentpy as _
from pathlib import Path
import math
import subprocess
import requests
import shutil
import websocket
import cups
from brother_ql.cli import cli as brotherql_cli

fileToPrint = "/tmp/sticker.png"


class PrinterConfig:

    def __init__(self,cupsPrinterUri, cupsOptions):
        self.cupsPrinterId = cupsPrinterUri
        self.cupsOptions = cupsOptions

    def print(self):
        if(self.cupsPrinterId):
            self.printCups()
        else:
            self.printBrotherCli()

    def printBrotherCli(self):
        try:
            brotherql_cli(["print", fileToPrint])
        except SystemExit:
            print("printed")

    def printCups(self):
        conn = cups.Connection ()
        conn.printFile(self.cupsPrinterId, fileToPrint, "",self.cupsOptions)
