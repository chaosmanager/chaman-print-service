#!/usr/bin/python3

import click
import sys
import fluentpy as _
from pathlib import Path
import math
import subprocess
import requests
import shutil
import websocket


class ChamanApi:
    verify = True

    def __init__(self, baseUrl, token, printerId, stickerWidth, stickerHeight):
        self.baseUrl = baseUrl
        self.token = token
        self.printerId = printerId
        self.stickerWidth = stickerWidth
        self.stickerHeight = stickerHeight

    def headers(self):
        return {"User-Token": self.token}

    def getPrintQueue(self):
        return requests.get(self.baseUrl + '/api/printer/'+self.printerId+'/print-queue/', headers=self.headers(), verify=self.verify).json()

    def stickerSize(self):
        r = []
        if (self.stickerWidth):
            r.append("width="+self.stickerWidth)
        if (self.stickerHeight):
            r.append("height="+self.stickerHeight)
        return "&".join(r)

    def getSticker(self, job):

        url = self.baseUrl + '/api/item/' + job['itemId'] + "/stickerImg?" + self.stickerSize()
        r = requests.get(url, headers=self.headers(), verify=self.verify, stream=True)
        r.raw.decode_content = True
        with open("/tmp/sticker.png", 'wb') as f:
            shutil.copyfileobj(r.raw, f)

    def markPrinted(self, job):
        return requests.put(self.baseUrl + '/api/print-job/' + job['id'] + "/status?status=Printed",
                            headers=self.headers(), verify=self.verify).json()

    def websocketUrl(self):
        return self.baseUrl.replace("https://", "wss://").replace("http://", "ws://") + "/api/event/printer/"+self.printerId

    def webSocket(self, on_message, on_close):
        websocket.enableTrace(True)
        return websocket.WebSocketApp(self.websocketUrl(),
                                      on_message=on_message,
                                      on_close=on_close,
                                      header=["User-Token: " + self.token]
                                      )
