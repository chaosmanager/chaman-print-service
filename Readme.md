# Chaman sticker print server

install requirements with 

```bash
pip install -r requirements.txt
```

## example

### bother

```bash
export BROTHER_QL_BACKEND=linux_kernel
export BROTHER_QL_PRINTER="file:///dev/usb/lp0"                                                                                                   
export BROTHER_QL_MODEL=QL-580N
export BROTHER_QL_LABEL=62
source ./venv/bin/activate;
python ./main.py --chaman-base-url=https://chaman.host.tld --chaman-token azertyuiop1234567890 --chaman-printer-id 01234567-aaaa-bbbb-cccc-dddddddddddd
```

### cups

```bash
source ./venv/bin/activate;
python ./main.py --chaman-base-url=https://chaman.host.tld --chaman-token azertyuiop1234567890 --chaman-printer-id 01234567-aaaa-bbbb-cccc-dddddddddddd --cups-printer PDF
```
